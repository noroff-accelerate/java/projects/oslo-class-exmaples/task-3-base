package main.java.factories;
// Imports
import main.java.characters.abstractions.CharacterCategory;
/*
 This factory exists to be responsible for creating new enemies.
 Object is replaced with Character as a return type when refactored to be good OO design.
*/
// TODO Once characters can be made with the right compositions, then implement the factory
public class CharacterFactory {
    public Object getCharacter(CharacterCategory characterType) {
        switch(characterType) {
            default:
                return null;
        }
    }
}
