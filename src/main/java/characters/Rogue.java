package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Leather;
import main.java.items.weapons.abstractions.WeaponCategory;
/*
 Rogues are stealthy combatants of the shadows.
 They wield bladed weapons with great agility and dispatch their enemies swiftly.
*/
public class Rogue {
    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Melee;
    public final ArmorType ARMOR_TYPE = ArmorType.Leather;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.BladeWeapon;

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.ROGUE_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.ROGUE_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.ROGUE_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseAttackPower = CharacterBaseStatsOffensive.ROGUE_MELEE_ATTACK_POWER;

    // Active trackers and flags
    private double currentHealth;
    private Boolean isDead = false;

    // Constructor
    public Rogue() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return baseHealth;
    }  // Needs alteration after equipment is in

    public double getCurrentHealth() {
        return currentHealth;
    }

    public Boolean getDead() {
        return isDead;
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Leather armor) {

    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Object weapon) {

    }

    // Character behaviours

    /**
     * Damages the enemy
     */
    public double attackWithBladedWeapon() {
        return 0; // Replaced with actual damage amount based on calculations
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        return 0; // Return damage taken after damage reductions, based on type of damage.
    }
}
