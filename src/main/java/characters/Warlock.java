package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Cloth;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.DamagingSpell;
import main.java.spells.abstractions.SpellCategory;
/*
 Warlocks are masters of chaos, entropy, and death. They were once mages but were corrupted by power.
 They can conjure up pure chaos energy to destroy their enemies.
*/
public class Warlock {
    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Caster;
    public final ArmorType ARMOR_TYPE = ArmorType.Cloth;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Magic;
    private DamagingSpell damagingSpell;

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.WARLOCK_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.WARLOCK_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.WARLOCK_BASE_MAGIC_RES; // Magic armor
    // Base stats offensive
    private double baseMagicPower = CharacterBaseStatsOffensive.WARLOCK_MAGIC_POWER;

    // Active trackers and flags
    private double currentHealth;
    private Boolean isDead = false;

    // Constructor
    public Warlock() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;
    }

    public Warlock(DamagingSpell damagingSpell) {
        this.currentHealth = baseHealth;
        this.damagingSpell = damagingSpell;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return baseHealth;
    }  // Needs alteration after equipment is in

    public double getCurrentHealth() {
        return currentHealth;
    }

    public Boolean getDead() {
        return isDead;
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Cloth armor) {

    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Object weapon) {

    }

    // Character behaviours

    /**
     * Damages the enemy with its spells
     */
    public double castDamagingSpell() {
        return 0; // Replaced with actual damage amount based on calculations
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        return 0; // Return damage taken after damage reductions, based on type of damage.
    }
}
