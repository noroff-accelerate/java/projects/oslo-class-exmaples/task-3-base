package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.armor.Cloth;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.ShieldingSpell;

/*
 Priest are the servants of the light and goodness.
 They use holy magic to heal and shield allies.
 As a support class they only have defensive stats.
*/
public class Priest {
    // Metadata
    public final CharacterCategory CHARACTER_CAT = CharacterCategory.Support;
    public final ArmorType ARMOR_TYPE = ArmorType.Cloth;
    public final WeaponCategory WEAPON_TYPE = WeaponCategory.Magic;
    private ShieldingSpell shieldingSpell;

    // Base stats defensive
    private double baseHealth = CharacterBaseStatsDefensive.PRIEST_BASE_HEALTH;
    private double basePhysReductionPercent = CharacterBaseStatsDefensive.PRIEST_BASE_PHYS_RED; // Armor
    private double baseMagicReductionPercent = CharacterBaseStatsDefensive.PRIEST_BASE_MAGIC_RES; // Magic armor
    // Active trackers and flags
    private double currentHealth;
    private Boolean isDead = false;

    // Constructor
    public Priest() {
        // When the character is created it has maximum health (base health)
        this.currentHealth = baseHealth;
    }

    public Priest(ShieldingSpell shieldingSpell) {
        this.currentHealth = baseHealth;
        this.shieldingSpell = shieldingSpell;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        return baseHealth;
    }  // Needs alteration after equipment is in

    public double getCurrentHealth() {
        return currentHealth;
    }

    public Boolean getDead() {
        return isDead;
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, modifying stats.
     * @param armor
     */
    public void equipArmor(Cloth armor) {

    }

    /**
     * Equips a weapon to the character, modifying stats.
     * @param weapon
     */
    public void equipWeapon(Object weapon) {

    }

    // Character behaviours

    /**
     * Heals the party member, increasing their current health.
     * @param partyMember
     */
    public void healPartyMember(Object partyMember) {

    }

    /**
     * Shields a party member for a percentage of their maximum health.
     * @param partyMemberMaxHealth
     */
    public double shieldPartyMember(double partyMemberMaxHealth) {
        return 0; // Return calculated shield value
    }

    /**
     * Takes damage from an enemy's attack.
     * @param incomingDamage
     * @Param damageType
     */
    public double takeDamage(double incomingDamage, String damageType) {
        return 0; // Return damage taken after damage reductions, based on type of damage.
    }
}
