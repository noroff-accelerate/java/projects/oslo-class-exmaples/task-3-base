package main.java.items.armor;
// Imports
import main.java.basestats.ArmorStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;

public class Cloth {
    // Stat modifiers
    private double healthModifier = ArmorStatsModifiers.CLOTH_HEALTH_MODIFIER;
    private double physRedModifier = ArmorStatsModifiers.CLOTH_PHYS_RED_MODIFIER;
    private double magicRedModifier = ArmorStatsModifiers.CLOTH_MAGIC_RED_MODIFIER;
    // Rarity
    private final double rarityModifier;

    // Public properties
    public double getHealthModifier() {
        return healthModifier;
    }

    public double getPhysRedModifier() {
        return physRedModifier;
    }

    public double getMagicRedModifier() {
        return magicRedModifier;
    }

    public double getRarityModifier() {
        return rarityModifier;
    }

    // Constructors
    public Cloth(double itemRarity) {
        this.rarityModifier = itemRarity;
    }

}
