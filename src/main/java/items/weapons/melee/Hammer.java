package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;

public class Hammer {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.HAMMER_ATTACK_MOD;
    // Rarity
    private ItemRarity rarity;

    // Public properties
    public ItemRarity getRarity() {
        return rarity;
    }

    // Constructors
    public Hammer() {
        this.rarity = ItemRarity.Common;
    }

    public Hammer(ItemRarity rarity) {
        this.rarity = rarity;
    }
}
