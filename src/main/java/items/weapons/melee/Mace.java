package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;

public class Mace {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.MACE_ATTACK_MOD;
    // Rarity
    private ItemRarity rarity;

    // Public properties
    public ItemRarity getRarity() {
        return rarity;
    }

    // Constructors
    public Mace() {
        this.rarity = ItemRarity.Common;
    }

    public Mace(ItemRarity rarity) {
        this.rarity = rarity;
    }
}
