package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;

public class Axe {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.AXE_ATTACK_MOD;
    // Rarity
    private ItemRarity rarity;

    // Public properties
    public ItemRarity getRarity() {
        return rarity;
    }

    // Constructors
    public Axe() {
        this.rarity = ItemRarity.Common;
    }

    public Axe(ItemRarity rarity) {
        this.rarity = rarity;
    }
}
