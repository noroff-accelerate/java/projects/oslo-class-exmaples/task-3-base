package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;

public class Dagger {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.DAGGER_ATTACK_MOD;
    // Rarity
    private ItemRarity rarity;

    // Public properties
    public ItemRarity getRarity() {
        return rarity;
    }

    // Constructors
    public Dagger() {
        this.rarity = ItemRarity.Common;
    }

    public Dagger(ItemRarity rarity) {
        this.rarity = rarity;
    }
}
