package main.java.items.weapons.melee;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;

public class Sword {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.SWORD_ATTACK_MOD;
    // Rarity
    private ItemRarity rarity;

    // Public properties
    public ItemRarity getRarity() {
        return rarity;
    }

    // Constructors
    public Sword() {
        this.rarity = ItemRarity.Common;
    }

    public Sword(ItemRarity rarity) {
        this.rarity = rarity;
    }
}
