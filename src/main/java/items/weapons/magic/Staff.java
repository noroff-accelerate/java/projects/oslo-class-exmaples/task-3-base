package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;

public class Staff {
    // Stat modifiers
    private double magicPowerModifier = WeaponStatsModifiers.STAFF_MAGIC_MOD;
    // Rarity
    private ItemRarity rarity;

    // Public properties
    public ItemRarity getRarity() {
        return rarity;
    }

    // Constructors
    public Staff() {
        this.rarity = ItemRarity.Common;
    }

    public Staff(ItemRarity rarity) {
        this.rarity = rarity;
    }
}
