package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;

public class Wand {
    // Stat modifiers
    private double magicPowerModifier = WeaponStatsModifiers.WAND_MAGIC_MOD;
    // Rarity
    private ItemRarity rarity;

    // Public properties
    public ItemRarity getRarity() {
        return rarity;
    }

    // Constructors
    public Wand() {
        this.rarity = ItemRarity.Common;
    }

    public Wand(ItemRarity rarity) {
        this.rarity = rarity;
    }
}
