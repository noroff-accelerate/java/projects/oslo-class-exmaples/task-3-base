package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;

public class Crossbow {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.CROSSBOW_ATTACK_MOD;
    // Rarity
    private ItemRarity rarity;

    // Public properties
    public ItemRarity getRarity() {
        return rarity;
    }

    // Constructors
    public Crossbow() {
        this.rarity = ItemRarity.Common;
    }

    public Crossbow(ItemRarity rarity) {
        this.rarity = rarity;
    }
}
