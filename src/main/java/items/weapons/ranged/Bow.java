package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;

public class Bow {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.BOW_ATTACK_MOD;
    // Rarity
    private ItemRarity rarity;

    // Public properties
    public ItemRarity getRarity() {
        return rarity;
    }

    // Constructors
    public Bow() {
        this.rarity = ItemRarity.Common;
    }

    public Bow(ItemRarity rarity) {
        this.rarity = rarity;
    }
}
