package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.abstractions.ItemRarity;

public class Gun {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.GUN_ATTACK_MOD;
    // Rarity
    private ItemRarity rarity;

    // Public properties
    public ItemRarity getRarity() {
        return rarity;
    }

    // Constructors
    public Gun() {
        this.rarity = ItemRarity.Common;
    }

    public Gun(ItemRarity rarity) {
        this.rarity = rarity;
    }
}
